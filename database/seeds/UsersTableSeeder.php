<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $user = new App\User(['name'=>'unregistered','email'=>'nobody@nothing.cz','password'=>bcrypt('test')]);
	    $user->save();
    }
}
