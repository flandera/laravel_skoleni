<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class'=>'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('text', 'Text:') !!}
    {!! Form::textarea('text', null, ['class'=>'form-control']) !!}
</div>
{!! Form::hidden('author_id' , Auth::user()->id) !!}
<div class="form-group">
    {!! Form::submit($text, ['class' => 'btn btn-primary form-control']) !!}
</div>
