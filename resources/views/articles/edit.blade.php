@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
           <h1>Změnit článek</h1>
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                        {!! Form::model($article, ['method'=>'PATCH', 'action' => ['ArticlesController@update', $article->id]]) !!}
                           @include('articles._form', ['text' => 'Změnit'])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
