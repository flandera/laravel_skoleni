@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
           <h1>Vytvořit článek</h1>
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                        {!! Form::open(['url' => 'articles']) !!}
                            @include('articles._form', ['text' => 'Vytvořit'])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
