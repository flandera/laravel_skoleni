<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
	/**
	 *
	 * @var array
	 */
	protected $fillable = [
		'title', 'text', 'author_id'
	];

	public function author(){
		return $this->belongsTo('App\User');
	}
}
